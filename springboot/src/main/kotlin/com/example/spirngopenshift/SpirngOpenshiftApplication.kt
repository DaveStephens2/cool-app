package com.example.spirngopenshift

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpirngOpenshiftApplication

fun main(args: Array<String>) {
	runApplication<SpirngOpenshiftApplication>(*args)
}


### s2i image

#### Getting Base s2i image - BROKEN!

- Import with `oc import-image ubi8/openjdk-11 --from=registry.access.redhat.com/ubi8/openjdk-11 --confirm`
- From [here](https://catalog.redhat.com/software/containers/ubi8/openjdk-11/5dd6a4b45a13461646f677f4?container-tabs=overview&gti-tabs=unauthenticated)


```
docker pull  registry.access.redhat.com/ubi8/openjdk-11:1.3
docker save -o ubi8_openjdk-11_1.3.image registry.access.redhat.com/ubi8/openjdk-11:1.3
```

Source code is distributed as an image for some reason. It is described [here](https://catalog.redhat.com/software/containers/ubi8/openjdk-11/5dd6a4b45a13461646f677f4?tag=1.3&container-tabs=gti&gti-tabs=get-the-source)

```
minishift ssh 
sudo yum install skopeo
skopeo copy docker://registry.access.redhat.com/ubi8/openjdk-11:1.3-3.1595332543-source dir:$HOME/TEST
```

Note: I could not get this to work! Only seems to work on a redhat OS... 

#### Creating Custom Spring Boot build image





#### Building with s2i

```
& minishift oc-env | Invoke-Expression
& minishift docker-env | Invoke-Expression
```

`s2i.exe build . registry.access.redhat.com/ubi8/openjdk-11:1.3 springboot-hello`

#### Playing with the built container

- Get shell with `docker run --rm -ti -p 8080:8080 springboot-hello /bin/bash`
- s2i scripts are in `/usr/local/s2i`



JBOSS_CONTAINER_S2I_CORE_MODULE=/opt/jboss/container/s2i/core/
JBOSS_CONTAINER_JAVA_S2I_MODULE=    - /opt/jboss/container/java/s2i/
DEPLOYMENTS_DIR                     - Code from the git repo  

maven_s2i_build                     - Function to compile code, defined in /opt/jboss/container/maven/s2i/maven-s2i

JAVA_APP_DIR=/deployments           - App for java?
${JBOSS_CONTAINER_JAVA_RUN_MODULE}/run-java.sh=/opt/jboss/container/java/run/run-java.sh   - Runs the java
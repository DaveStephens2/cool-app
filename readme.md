# Openshift things


## Openshift images

- These are the base images used to build our containers.  They usually come from the redhat container registry. 


- Here is a github page about the python images: https://github.com/sclorg/s2i-python-container
- Looks like I can get a centos version from: https://hub.docker.com/u/centos
- Common centos s2i base image: https://hub.docker.com/r/centos/s2i-base-centos7  (https://github.com/sclorg/s2i-base-container)
- No centos jdk image exists but this fabric8 one may be what i need: https://hub.docker.com/r/fabric8/s2i-java (https://github.com/fabric8io-images/s2i/tree/master/java/images/centos-java11)

### Image Streams used:

- nginx - We will be using this image directly so it will be referenced in the deployment layer. This image: `registry.redhat.io/rhel8/nginx-116`
- springboot-s2i - S2I builder for spring boot apps. There is no openshift image for doing this so I built my own.

### Builds

oc create -f springboot-s2i-bc.yaml            # Builds the s2i image for building spring boot apps
oc create -f spring-hello-is.yaml              # Creates a place to store the spring-hello images
oc create -f spring-hello-bc.yaml              # Builders for spring-hello images 
oc create -f cool-app-dc.yaml                  # How to deploy spring-hello images 
oc create -f cool-app-svc.yaml                 # Grouping of containers into a service  
oc create -f cool-app-route.yaml               # expose port 8080 to point at the cool-app service


### From a template

- Create instance of app with: `oc process -f cool_app_template.yaml | oc apply -f -`

```powershell
oc delete-project test
oc new-project test
oc create secret generic nginx-certs --from-file=C:\Users\DaveS\Desktop\openshift-code\nginx\server-certs   # certs for nginx 
oc process -f  C:\Users\DaveS\Desktop\springboot-s2i\springboot-s2i-template.yaml | oc apply -f -           # s2i builders
oc process -f  C:\Users\DaveS\Desktop\openshift-code\cool_app_template.yaml | oc apply -f -    
```


## Liveness / readyness probes

- Spring boot 2.3 adds support described here: https://spring.io/blog/2020/03/25/liveness-and-readiness-probes-with-spring-boot


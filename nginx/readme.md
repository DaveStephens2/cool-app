
## Base image

- The nginx container is built as a s2i


```
docker login registry.redhat.io
Username: lspjenbeaazarwlatf
Password: 9v57/+?E7*5GmvAc
Login Succeeded!

docker pull registry.redhat.io/rhel8/nginx-116:1-65
docker save -o rhel8_nginx-16_1-65.image registry.redhat.io/rhel8/nginx-116:1-65 
```

Base nginx image is here

- Image documentation https://catalog.redhat.com/software/containers/rhel8/nginx-116/5d400ae7bed8bd3809910782?container-tabs=overview


## Generate Certs


From this blog: https://codergists.com/redhat/containers/openshift/2019/09/19/securing-http-containers-with-ssl-client-certificates-and-an-nginx-sidecar.html

```bash 
# Generate the Root CA certificate
openssl genrsa -aes256 -passout pass:password -out ca.pass.key 4096
openssl rsa -passin pass:password -in ca.pass.key -out ca.key
openssl req -new -x509 -days 365 -key ca.key -out ca.crt
#rm ca.pass.key

# Create the Server Key, CSR, and Certificate - This will be used by the NGINX sidecar container (the Server)
openssl genrsa -aes256 -passout pass:password -out server.pass.key 4096
openssl rsa -passin pass:password -in server.pass.key -out server.key
openssl req -new -key server.key -out server.csr

# We're self signing our own server cert here.  This is a no-no in production.
openssl x509 -req -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt

# Create the Client Key and CSR - All of this is required for the client (the web browser client)
openssl genrsa -aes256 -passout pass:password -out client.pass.key 4096
openssl rsa -passin pass:password -in client.pass.key -out client.key
openssl req -new -key client.key -out client.csr

# Sign the client certificate with our CA cert.  Unlike signing our own server cert, this is what we want to do.
openssl x509 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out client.crt

# Bundle the private key & cert for end-user client use
cat client.key client.crt ca.crt > client.full.pem

# Bundle client key into a PFX file - this is what will be imported to the browser to use as a client certifcate
openssl pkcs12 -export -out client.full.pfx -inkey client.key -in client.full.pem -certfile ca.crt
```

> During the certificate requests you will be prompted for more info, I mainly focused on using a Common Name that was appropriate for each scenario (i.e. Root, Server, and Client). Any ‘extra’ attributes like challenge password were left blank.


package com.example.springauth

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController


@SpringBootApplication
class SpringAuthApplication

fun main(args: Array<String>) {
	runApplication<SpringAuthApplication>(*args)
}

@Configuration
@EnableWebSecurity
class SecurityConfig: WebSecurityConfigurerAdapter() {
	override fun configure(http: HttpSecurity) {
		// Allow all things without authentication
		http
			.authorizeRequests()
			.antMatchers("/**").permitAll()
	}
}

@RestController
@CrossOrigin
class AuthController{
	protected val logger = LoggerFactory.getLogger(javaClass)

	@GetMapping("/auth")
	fun authenticateUser(): String {
		logger.info("User is totally authenticated...")
		return "AUTHENTICATED!"
	}
}